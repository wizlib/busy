# BusyML  markup

Every item in a Busy collection is created and stored using a specific form of markup we call "BusyML".

Loosely, an item's markup contains one or more space-separated **words**, and those works that start with a **marker** indicate some special data field.

The idea is to concentrate useful information in a way that's easy to remember and type.

The markers are:

- `#` - tag
- `%` - data value
- `!` - timing-related data
- `@` - url (only one allowed)
- `>` - repeat timing, special because the marker itself can exist as its own word (i.e. have a space after) - only one allowed

All words without markers that appear before the repeat marker are considered the "base" description, and represent the human-readable action or topic of the item.

## Example

`Check new Mac prices #shopping #weekend %i88 !e14 @https://apple.com/store > repeat on saturday'

- The base is "Check new Mac prices"
- Tags are `shopping` and `weekend`
- One data item `i88` (specifically might refer to a GitLab issue iid if using the `busy-gitlab` integration)
- 14 minutes of elapsed time
- Related url is the Apple store
- When done, the task will repeat next Saturday

## Tags

Items can have tags, which are space-separated hashtags in the markup. An item can have no tags, one tag, or more than one tag. For example the following item has the tag "errands":

```
go to the supermarket #errands
```

The only punctuation that tags can contain is the hyphen ("-").

## Data values

Data values are used by integrations to correlate to outside systems. They start with `%` followed immediately by a 1-letter reference to the data value being tracked and (optionally) a value.

It's possible to filter by data vals.

`busy list val:x45`

Use the `view` command to view data values in isolation.

`busy view --fields val:x`

## Timing-related data

The [time tracking subsystem](time-tracking.md) inserts words starting with `!` into tasks. Leave them alone.

## URL

Useful place to bookmark a single URL related to the item

## Repeat timing

Described on the [planning](planning.md) page.

## Ready to get Busy?

Find out how to edit data directly in [Editing and Storage](system.md).

