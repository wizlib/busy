from types import SimpleNamespace

from busy.model.item import Item
from busy.test_case import BusyTestCase


def obj(markup):
    return SimpleNamespace(**vars(Item(markup)))


class TestItem(BusyTestCase):

    def test_base(self):
        o = obj('gh')
        self.assertEqual('gh', o.base)

    def test_tag(self):
        o = obj('a #b')
        self.assertEqual('b', o.tag.b)

    def test_val(self):
        o = obj('gh %i8')
        self.assertEqual('8', o.val.i)
