from random import seed
from unittest import TestCase
from unittest.mock import Mock, patch

from busy.command.drop_and_pop_command import DropCommand
from busy.command.drop_and_pop_command import PickCommand
from busy.command.drop_and_pop_command import PopCommand
from busy.model.collection.todo_collection import TodoCollection
from busy.model.item import Item
from busy.test_case import BusyTestCase


class TestCommandDropAndPop(BusyTestCase):

    def test_drop(self):
        o = TodoCollection([Item('#a'), Item('#b'), Item('#c')])
        s = Mock()
        s.storage.get_collection.return_value = o
        c = DropCommand(s)
        c.execute()
        self.assertEqual([str(x) for x in o], ['#b', '#c', '#a'])
        self.assertTrue(o.changed)

    def test_drop_respond_nothing(self):
        o = TodoCollection()
        s = Mock()
        s.storage.get_collection.return_value = o
        c = DropCommand(s)
        c.execute()
        self.assertEqual(c.status, "")

    def test_pop(self):
        o = TodoCollection([Item('#a'), Item('#b'), Item('#c')])
        s = Mock()
        s.storage.get_collection.return_value = o
        c = PopCommand(s)
        c.execute()
        self.assertEqual([str(x) for x in o], ['#c', '#a', '#b'])
        # self.assertTrue(o.changed)

    def test_pick(self):
        o = TodoCollection([Item('#a'), Item('#b'), Item('#c')])
        s = Mock()
        s.storage.get_collection.return_value = o
        c = PickCommand(s, filter=['b', 'c'])
        seed(1)
        c.execute()
        self.assertEqual([str(x) for x in o], ['#b', '#a', '#c'])

    def test_pick_output(self):
        o = TodoCollection([Item('#a'), Item('#b'), Item('#c')])
        s = Mock()
        s.storage.get_collection.return_value = o
        c = PickCommand(s, filter=['b', 'c'])
        o = c.execute()
        self.assertEqual(None, o)

    def test_pop_again(self):
        o = TodoCollection([Item('#a'), Item('x #b'), Item('y #b')])
        s = Mock()
        s.storage.get_collection.return_value = o
        c = PopCommand(s, filter=['b'])
        c.execute()
        self.assertEqual([str(x) for x in o], ['x #b', 'y #b', '#a'])

    def test_drop_again(self):
        o = TodoCollection([Item('v #a'), Item('x #a'), Item('y #b')])
        s = Mock()
        s.storage.get_collection.return_value = o
        c = DropCommand(s, filter=['a'])
        c.execute()
        self.assertEqual([str(x) for x in o], ['y #b', 'v #a', 'x #a'])

    def test_pick_whole_collection(self):
        o = TodoCollection([Item('#a'), Item('#b'), Item('#c'), Item('#d')])
        s = Mock()
        s.storage.get_collection.return_value = o
        c = PickCommand(s)
        seed(1)
        c.execute()
        self.assertEqual([str(x) for x in o], ['#b', '#a', '#c', '#d'])
