
from busy import BusyApp
from busy.model.collection.plan_collection import PlanCollection
from busy.model.collection.todo_collection import TodoCollection
from busy.model.item import Item
from test import BusyMockIntegrationTestCase


class TestCommandGet(BusyMockIntegrationTestCase):

    def test_parse_run(self):
        o = TodoCollection([Item('a %x5')])
        a = BusyApp()
        a.storage = self.mock_storage(o, PlanCollection())
        with self.patchout() as o, self.patcherr():
            a.parse_run('get', 'mock_integration')
        o.seek(0)
        self.assertEqual('5', o.read())
