MCP

```json
{"jsonrpc": "2.0", "id": 1, "method": "initialize", "params": {"protocolVersion": "2024-11-05", "capabilities": {"roots": {"listChanged": true}, "sampling": {}}, "clientInfo": {"name": "TestClient", "version": "1.0.0"}}}

{"jsonrpc": "2.0", "method": "notifications/initialized"}

{"jsonrpc": "2.0", "id": "123",  "method": "ping"}

{"jsonrpc": "2.0", "id": 2, "method": "tools/list"}

{"jsonrpc": "2.0", "id": 3, "method": "tools/call", "params": {"name": "perform", "arguments": {"command": "queues"}}}
```

PYTHONPATH=/Users/personal/gitlab.com/steamwiz/busy /Users/personal/gitlab.com/steamwiz/busy/.venv/bin/python -m busy --config /Users/personal/gitlab.com/steamwiz/busy/sandbox/config.yml mcp