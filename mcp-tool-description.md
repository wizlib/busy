# Busy Tool Guide for LLM Integration

## Overview
Busy is a terminal-based task management tool that emphasizes single-task focus and keyboard-driven operation. When accessed through an MCP server, commands are used directly without the 'busy' prefix (e.g., `add` rather than `busy add`).

## Data Model
- **Items**: Basic units (tasks, topics, etc.) stored in queues
- **States**: Items exist in one of three states:
  - `todo`: Current/active items
  - `done`: Completed items (includes completion date)
  - `plan`: Future items (includes planned date)
- **Queues**: Named collections of items (default is "tasks")

## BusyML Markup
Items use a specialized markup format:
- `#tag`: Tags for categorization
- `%key123`: Data values for external system integration
- `!e30`: Time tracking (e minutes elapsed)
- `@url`: Associated URL
- `> timing`: Repeat schedule

Example:
```
Write documentation #docs %i123 !e45 @https://docs.example.com > weekly
```

## Key Commands

### Viewing Tasks
- `simple`: Show top task in simplified format
- `list`: Show all tasks with sequence numbers
- `list -s done`: View completed tasks
- `list -s plan`: View planned tasks
- `describe`: Show full markup details
- `view`: Output specific fields (primarily for data values)

### Managing Tasks
- `add "Task description"`: Create new task
- `done`: Complete current task
- `delete [numbers]`: Remove tasks
- `defer`: Move task to future date
  - Accepts date formats: YYYY-MM-DD, MM-DD, "tomorrow", "4d"
- `pop [number]`: Move task to top
- `drop [number]`: Move task to bottom

### Filtering
- By sequence number: `list 1-3`
- By tag: `list project1`
- By date range: 
  - `list -s done donemin:2024-01-01`
  - `list -s plan planmax:2024-12-31`
- By data value: `list val:i123`

## Output Interpretation
- List command shows sequence numbers and full task details
- Status line shows total items and time value (if applicable)
- Error messages indicate command syntax issues
- Time tracking appears in status as total minutes × multiplier

## Common Usage Patterns
1. Check current task: `simple`
2. View all tasks: `list`
3. Complete task: `done`
4. Add and prioritize: 
   ```
   add "New important task"
   pop -
   ```
5. Defer task:
   ```
   defer
   When? tomorrow
   ```

## Tips for LLM Integration
- Always interpret sequence numbers from most recent `list` output
- Time tracking is automatic for tasks in default queue
- Commands without filters typically operate on top task
- Status messages provide important context (item counts, time values)
- When filtering, multiple criteria use AND logic between types (numbers/tags) but OR logic within types